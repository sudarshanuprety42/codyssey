class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string "name", :limit => 25, :null => false
      t.string "description", :limit => 1000, :null => true
      t.timestamps
    end
  end
end
