class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string "name", :limit => 50, :null => false
      t.string "address", :null => false
      t.string "age", :null => false
      t.integer "salary", :null => false
      t.references :project
      t.timestamps
    end
  end
end
