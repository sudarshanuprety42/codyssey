# :nodoc:
class ProjectsController < ApplicationController
  def index
    @projects = Project.all
  end

  def new
    @project = Project.new
  end

  def create
    project = Project.new(project_save_params)
    # project.save ? render 'index' : render 'new'
    if project.save
      flash[:success] = "Project Saved!"
      redirect_to root_path
    else
      flash[:alert] = "Invalid project name!"
      redirect_to :action => 'new'
    end
  end

    def destroy
      project = Project.find(project_params[:id])
      project.destroy
      flash[:notice] = "Project Deleted"
      redirect_to root_path
    end

  def edit
    @project = Project.find(project_params[:id])
  end

  def update
    project = Project.find(project_params[:id])
    if project.update(project_save_params)
      flash[:success] = "Project Updated"
      redirect_to root_path
    else
      flash[:alert] = "Error updating project!"
      redirect_to edit_project_path
    end

  end


  private

  def project_save_params
    params.require(:project).permit(:name, :description)
  end

  def project_params
    params.permit(:id)
  end

end
