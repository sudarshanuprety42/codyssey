class EmployeesController < ApplicationController
  def new
    @employee = Employee.new

  end

  def create
    employee = Employee.new(employee_save_params)
    if employee.save
      flash[:success] = "Employee Saved!"
      redirect_to employees_path
    else
      flash[:alert] = employee.errors.messages
      redirect_to :action => 'new'
    end
  end

  def index
    @employees = Employee.all
  end

  def destroy
    employee = Employee.find(employee_params[:id])
    employee.destroy
    flash[:notice] = "Employee Deleted"
    redirect_to employees_path
  end

  def edit
    @employee = Employee.find(employee_params[:id])
  end

  def update
    employee = Employee.find(employee_params[:id])
    if employee.update(employee_save_params)
      flash[:success] = "Employee Updated"
      redirect_to employees_path
    else
      flash[:alert] = employee.errors.messages
      redirect_to edit_employee_path
    end

  end

  private

  def employee_save_params
    params.require(:employee).permit(:name, :address, :age, :salary, :project_id)
  end

  def employee_params
    params.permit(:id)
  end

end
