class Employee < ApplicationRecord
  validates :name, presence:{message: "Enter Name"}, length: { maximum: 25 }
  validates :address, presence:{message: "Enter address"}
  validates :age, presence:{message: "Enter age"}, numericality: { only_integer: true, message: "Invalid age" }
  validates :salary, presence:{message: "Enter salary"}, numericality: { only_integer: true, message: "Invalid salary" }

  belongs_to :project
  validates :project, presence:{message: "Select project"}
end
