class Project < ApplicationRecord
  validates :name, presence:true, length: { maximum: 25 }
  has_many :employees
end
